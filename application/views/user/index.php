

   

    
        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800"><?= $title;?></h1>


          <div class= "row">
              <div class= "col-lg-6">
                <?= $this->session->flashdata('message'); ?>

              </div>

          </div>




          <div class="card mb-3" style="max-width: 1000px;">
  <div class="row no-gutters">
    <div class="col-md-3">
      <img src="<?= base_url('assets/img/profile/') .$user['image'];?>" class="card-img" >
    </div>
    <div class="col-md-9">
      <div class="card-body">

        <div class="form-group row">
    <label for="email" class="col-sm-2 col-form-label">Nama Saya</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="name" name= "name" value="<?= $user['name']; ?>"readonly>
     </div>
  </div>
        <div class="form-group row">
    <label for="email" class="col-sm-2 col-form-label">Email</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="email" name= "email" value="<?= $user['email']; ?>"readonly>
    </div>
  </div>

  <div class="form-group row">
    <label for="no_telp" class="col-sm-2 col-form-label">No Telephon </label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="no_telp" name= "no_telp" value="<?= $user['no_telp']; ?>"readonly>
      
    </div>
  </div>

  <div class="form-group row">
    <label for="no_telp" class="col-sm-7 col-form-label"></label>
    <div class="col-sm-5">
      <input type="text" class="form-control" id="date_created" name= "date_created" value="<?= "Member FKDK Since: ".date('d F Y', $user['date_created']); ?>"readonly>
      
    </div>
  </div>

     </div>
    </div>
  </div>
</div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

     