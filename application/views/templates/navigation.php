<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
* {box-sizing: border-box;}

body { 
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

#navbar {
  overflow: hidden;
  background-color:#ACB1AD;
  padding: 20px 5px;
  transition: 0.4s;
  position: fixed;
  width: 100%;
  top: 0;
  z-index: 19;
}

#navbar a {
  float: left;
  color: black;
  text-align: center;
  padding: 12px;
  text-decoration: none;
  font-size: 18px; 
  line-height: 25px;
  border-radius: 4px;
}

#navbar #logo {
  font-size: 35px;
  font-weight: bold;
  transition: 0.4s;
}

#navbar a:hover {
  background-color: #ddd;
  color: black;
}

#navbar a.active {
  background-color: dodgerblue;
  color: white;
}

#navbar-right {
  float: right;
}

@media screen and (max-width: 580px) {
  #navbar {
    padding: 20px 10px !important;
  }
  #navbar a {
    float: none;
    display: block;
    text-align: left;
  }
  #navbar-right {
    float: none;
  }
}
</style>
</head>
<body>

<div id="navbar">
  <a class="fas fa-couch" href="#page-top" id="logo"></a>
  <div id="navbar-left">
    <a class="nav-link js-scroll-trigger" href="#page-top">Home</a>
    <a class="nav-link js-scroll-trigger" href="#about">About</a>
    <a class="nav-link js-scroll-trigger" href="#services">Service</a>
    <a class="nav-link js-scroll-trigger" href="#portfolio">Produk</a>
    <a class="nav-link js-scroll-trigger"href="#contact">Pemesanan</a>
     <a class="nav-link js-scroll-trigger"href="#information">Information</a>
    </div>
    <div id="navbar-right">
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form>
  </div>
</div>


<script>
// When the user scrolls down 80px from the top of the document, resize the navbar's padding and the logo's font size
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 80 || document.documentElement.scrollTop > 80) {
    document.getElementById("navbar").style.padding = "20px 10px";
    document.getElementById("logo").style.fontSize = "25px";
  } else {
    document.getElementById("navbar").style.padding = "15px 8px";
    document.getElementById("logo").style.fontSize = "35px";
  }
}
</script>

</body>
</html>
