

  <!-- About Section -->
  <section class="page-section bg-primary" id="about">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-8 text-center">
          <h2 class="text-white mt-0">Apa itu Zassava!?</h2>
          <div class="text-center" >
          <img src="img/1.jpeg" class="rounded" style="height: 120px; width: 80px;">
          </div>
          <hr class="divider light my-4">
          <p class="text-white-50 mb-4">Zassava adalah sebuah makanan ringan terbuat dari singkong.Cocok untuk menemani saat kamu sibuk dengan Tugas, bersantai dan bersama keluarga</p>
          <a class="btn btn-light btn-xl js-scroll-trigger" href="#services">Get Started!</a>
        </div>
      </div>
    </div>
  </section>

  <!-- Services Section -->
  <section class="page-section" id="services">
    <div class="container">
      <h2 class="text-center mt-0">Zassava Kuyyy</h2>
      <hr class="divider my-4">
      <div class="row">
        <div class="col-lg-3 col-md-6 text-center">
          <div class="mt-5">
            <i class="fas fa-4x fa-money-check-alt text-primary mb-4"></i>
            <h3 class="h4 mb-2">Price</h3>
            <p class="text-muted mb-0">Harga Terjangkau Mulai dari Rp. XXXX</p>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 text-center">
          <div class="mt-5">
            <i class="fas fa-4x fa-archive text-primary mb-4"></i>
            <h3 class="h4 mb-2">Package</h3>
            <p class="text-muted mb-0">Kemasan yang Menarik dan Elegan.</p>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 text-center">
          <div class="mt-5">
            <i class="fas fa-4x fa-grin text-primary mb-4"></i>
            <h3 class="h4 mb-2">Taste</h3>
            <p class="text-muted mb-0">Membuat Kamu nyaman dengan Rasanya yang Gurih!</p>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 text-center">
          <div class="mt-5">
            <i class="fas fa-4x fa-truck text-primary mb-4"></i>
            <h3 class="h4 mb-2">Delivery</h3>
            <p class="text-muted mb-0">Kami Siap mengantar ke Tempat Anda</p>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- Portfolio Section -->
  <section id="portfolio">
      <div class="container-fluid p-0">
      <div class="row no-gutters">
        <div class="col-lg-4 col-sm-6">
          <a class="portfolio-box" href="img/portfolio/fullsize/1.jpg">
            <img class="img-fluid" src="img/portfolio/thumbnails/1.jpg" alt="">
            <div class="portfolio-box-caption">
              <div class="project-category text-white-50">
                Category
              </div>
              <div class="project-name">
                Project Name
              </div>
            </div>
          </a>
        </div>
        <div class="col-lg-4 col-sm-6">
          <a class="portfolio-box" href="img/portfolio/fullsize/2.jpg">
            <img class="img-fluid" src="img/portfolio/thumbnails/2.jpg" alt="">
            <div class="portfolio-box-caption">
              <div class="project-category text-white-50">
                Category
              </div>
              <div class="project-name">
                Project Name
              </div>
            </div>
          </a>
        </div>
        <div class="col-lg-4 col-sm-6">
          <a class="portfolio-box" href="img/portfolio/fullsize/3.jpg">
            <img class="img-fluid" src="img/portfolio/thumbnails/3.jpg" alt="">
            <div class="portfolio-box-caption">
              <div class="project-category text-white-50">
                Category
              </div>
              <div class="project-name">
                Project Name
              </div>
            </div>
          </a>
        </div>
        <div class="col-lg-4 col-sm-6">
          <a class="portfolio-box" href="img/portfolio/fullsize/4.jpg">
            <img class="img-fluid" src="img/portfolio/thumbnails/4.jpg" alt="">
            <div class="portfolio-box-caption">
              <div class="project-category text-white-50">
                Category
              </div>
              <div class="project-name">
                Project Name
              </div>
            </div>
          </a>
        </div>
        <div class="col-lg-4 col-sm-6">
          <a class="portfolio-box" href="img/portfolio/fullsize/5.jpg">
            <img class="img-fluid" src="img/portfolio/thumbnails/5.jpg" alt="">
            <div class="portfolio-box-caption">
              <div class="project-category text-white-50">
                Category
              </div>
              <div class="project-name">
                Project Name
              </div>
            </div>
          </a>
        </div>
        <div class="col-lg-4 col-sm-6">
          <a class="portfolio-box" href="img/portfolio/fullsize/6.jpg">
            <img class="img-fluid" src="img/portfolio/thumbnails/6.jpg" alt="">
            <div class="portfolio-box-caption p-3">
              <div class="project-category text-white-50">
                Category
              </div>
              <div class="project-name">
                Project Name
              </div>
            </div>
          </a>
        </div>
      </div>
    </div>
  </section>


  <!-- Contact Section -->
  <section class="page-section" id="contact">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-8 text-center">
          <h2 class="mt-0">Pemesanan Produk</h2>
          <hr class="divider my-4">
          <p class="text-muted mb-5">Silahkan Klik Tombol Dibawah ini untuk Memesan</p>
          <a class="btn btn-primary" href="https://api.whatsapp.com/send?phone=6285717059061&text=Saya%0Atertarik%0Auntuk%0Aberlangganan%0AKIRIM.EMAIL role="button">Pemesanan</a>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-4 ml-auto text-center mb-5 mb-lg-0">
          <i class="fas fa-phone fa-3x mb-3 text-muted"></i>
          <div>+1 (202) 555-0149</div>
        </div>
        <div class="col-lg-4 mr-auto text-center">
          <i class="fas fa-envelope fa-3x mb-3 text-muted"></i>
         
          <a class="d-block" href="mailto:contact@yourwebsite.com">Rahmat@Zassava.com</a>
        </div>
      </div>
    </div>
  </section>


  <!-- Information Section -->
  <section class="page-section bg-primary" id="information">
    <div class="container">
     <section id="demos">
    <div class="row">
      <div class="large-12 columns">
        <div class="owl-carousel owl-theme">
          <div class="item">
            <img src="img/1.jpeg" >
          </div>
          <div class="item">
            <img src="img/2.jpeg" >
          </div>
          <div class="item">
            <img src="images/owl-3.png" >
          </div>
          <div class="item">
            <img src="images/owl-4.png" >
          </div>
          <div class="item">
            <img src="images/owl-5.png" >
          </div>
          <div class="item">
            <img src="images/owl-1.png" >
          </div>
          <div class="item">
            <img src="images/owl-2.png" >
          </div>
        </div>
      </div>
    </div>
  </section> 

      


    </div>
  </section>