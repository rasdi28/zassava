<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		is_logged_in();
	}
	
	public function index()
	{
		$data['title']= 'My Profile';
		$data['user'] = $this->db->get_where('user',['email'=>$this->session->userdata('email')])->row_array();
		$this->load->view('templates_admin/header', $data);
		$this->load->view('templates_admin/sidebar', $data);
		$this->load->view('templates_admin/topbar', $data);
		$this->load->view('user/index', $data);
		$this->load->view('templates_admin/footer');
		
	}

	public function edit()
	{
		$data['title']= 'Edit Profile';
		$data['user'] = $this->db->get_where('user',['email'=>$this->session->userdata('email')])->row_array();

		$this->form_validation->set_rules('name',' Full Name', 'required|trim');


		if($this->form_validation->run() == false){
		$this->load->view('templates_admin/header', $data);
		$this->load->view('templates_admin/sidebar', $data);
		$this->load->view('templates_admin/topbar', $data);
		$this->load->view('user/edit', $data);
		$this->load->view('templates_admin/footer');

		} else{
			$name = $this->input->post('name');
			$email = $this->input->post('email');
			$no_telp = $this->input->post('no_telp');

			//cek jika da gambar yang akan di upload

		$upload_image = $_FILES['image']['name'];
		
			if ($upload_image){
				$config['allowed_types'] = 'gif|jpg|png';
				$config['max_size']     = '2048';
				$config['upload_path'] = './assets/img/profile/';

				$this->load->library('upload', $config);

				 if ($this->upload->do_upload('image')){
				 	$old_image = $data['user']['image'];
				 	if($old_image != 'default.jpg') {
				 		unlink(FCPATH . 'assets/img/profile/' . $old_image);
				 	}



				 	$new_image = $this->upload->data('file_name');
				 	$this->db->set('image', $new_image);

				 }else{
				 	echo $this->upload->display_errors();
				 }



			}


			$this->db->set('name',$name);
			$this->db->set('no_telp',$no_telp);
			$this->db->where('email', $email);
			$this->db->update('user');

			$this->session->set_flashdata('message','<div class="alert alert-success" role="alert"> Your Profile Has been update
			</div>');
			redirect('user');

		}
		

	}


}
